import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class textPreview extends StatelessWidget {
  var detail = {};
  ScrollController _controller = new ScrollController();

  textPreview(this.detail);

  @override
  Widget build(BuildContext context) {
    return ListView(
        shrinkWrap: true,
        controller: _controller,
        physics: const AlwaysScrollableScrollPhysics(),
        children: <Widget>[
          for (var i = 0; i < detail["articles"].length; i++)
            Card(
                child: ListTile(
              title: Text(detail["articles"][i]["title"]),
            )),
        ]);
  }
}
