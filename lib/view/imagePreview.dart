import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class imagePreview extends StatelessWidget {
  var detail = [];
  ScrollController _controller = new ScrollController();

  imagePreview(this.detail);

  @override
  Widget build(BuildContext context) {
    return ListView(
        shrinkWrap: true,
        controller: _controller,
        physics: const AlwaysScrollableScrollPhysics(),
        children: <Widget>[
          for (var i = 0; i < detail.length; i++)
            Center(
                child: Image.network(detail[i]["urls"]["regular"],
                    width: 200, height: 190)),
        ]);
  }
}
