import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import './config/settings.dart';
import './view/imagePreview.dart';
import './view/textPreview.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'dart:html' as html;
import 'package:i18n_extension/i18n_extension.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'flutter spa',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'FLUTTER SPA'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var data = [];
  var datanews = {};
  var position = "";
  void fetchApi(String menu) async {
    setState(() {
      position = "loading";
    });
    if (menu == "image") {
      Navigator.pushNamed(context, "/"+menu);
      final response = await http.get(
        Uri.parse(getApi(0.0) + "/photos"),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization':
              'Client-ID zmrsZJfVG31La3wce7PkE57PBz_qa5R4QG8h7fbjoGE'
        },
      );
      setState(() {
        position = menu;
        data = jsonDecode(response.body);
      });
    }
    if (menu == "news") {
      final response = await http.get(
        Uri.parse("https://newsapi.org/v2/everything?q=bca&apiKey=014c73b2772b42c48b86781eab217388"),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
      );
      setState(() {
        position = menu;
        datanews = jsonDecode(response.body);
      });
    }
  }
  final ScrollController listScrollController = ScrollController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        body: Column(children: <Widget>[
          Center(
              child: Center(
            child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  OutlinedButton(
                    onPressed: () {
                      fetchApi("image");
                    },
                    child: Text("Image"),
                  ),
                  OutlinedButton(
                    onPressed: () {
                      fetchApi("news");
                    },
                    child: Text("News"),
                  )
                ]),
          )),

          if (position == "image") ...[
            Flexible(
                child: ListView.builder(
              padding: const EdgeInsets.all(10.0),
              itemBuilder: (context, index) => imagePreview(data),
              itemCount: data.length,
              reverse: false,
              controller: listScrollController,
            ))
          ],
          if (position == "news") ...[
            Flexible(
            child:textPreview(datanews),
            )
          ],
          if (position == "loading") ...[
            SpinKitRotatingCircle(
              color: Colors.blue,
              size: 50.0,
            )
          ]
        ]));
  }
}
